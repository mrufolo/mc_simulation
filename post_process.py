#%%
import numpy as np
import pandas as pd
from matplotlib import pyplot as plt
import matplotlib.cm as cm
import itertools
from simple_jobs import tools as tls

n_jobs = 2
n_generations = 4
colors = iter(cm.rainbow(np.linspace(0, 1, n_generations+1)))
dict_bound = {}
dict_MC = {}


#fig, axs = plt.subplots(n_jobs)
for jobs in range(n_jobs):
    #print(f'{jobs:03}')
    path = f'{jobs:03}_child'
    data_init = pd.read_parquet(path + '/input.parquet')
    if jobs == 0:
        dict_MC = {f'1_generation': pd.read_parquet(path + '/output_df.parquet')
        }
        for i in range(n_generations-1):
            path = path + '/000_child'
            print(path)
            dict_MC[f'{i+2}_generation'] = pd.read_parquet(path + '/output_df.parquet')
    elif jobs == n_jobs-1:
        dict_MC[f'1_generation'] = pd.concat([dict_MC[f'1_generation'],pd.read_parquet(path + '/output_df.parquet')], ignore_index=True)
        dict_bound[f'1_generation'] = tls.search_boundary(dict_MC[f'1_generation'])
        plt.scatter([ii['events_ALICE'] for ii in dict_bound[f'1_generation']],[ii['events_LHCb'] for ii in dict_bound[f'1_generation']], color =next(colors))
        for i in range(n_generations-1):
            path = path + '/000_child'
            print(path)
            dict_MC[f'{i+2}_generation'] = pd.concat([dict_MC[f'{i+2}_generation'],pd.read_parquet(path + '/output_df.parquet')], ignore_index=True)
            dict_bound[f'{i+2}_generation'] = tls.search_boundary(dict_MC[f'{i+2}_generation'])
            plt.scatter([ii['events_ALICE'] for ii in dict_bound[f'{i+2}_generation']],[ii['events_LHCb'] for ii in dict_bound[f'{i+2}_generation']], color =next(colors))
    else:
        dict_MC[f'1_generation'] = pd.concat([dict_MC[f'1_generation'],pd.read_parquet(path + '/output_df.parquet')], ignore_index=True)
        for i in range(n_generations-1):
            path = path + '/000_child'
            print(path)
            dict_MC[f'{i+2}_generation'] = pd.concat([dict_MC[f'{i+2}_generation'],pd.read_parquet(path + '/output_df.parquet')], ignore_index=True)

    #axs[jobs].scatter([ii['events_ALICE'] for ii in dict_bound[f'{i+2}_generation']],[ii['events_LHCb'] for ii in dict_bound[f'{i+2}_generation']], color =next(colors))
    
        
plt.scatter(1737,1735,c='black' )
plt.scatter(data_init['events_ALICE'],data_init['events_LHCb'],c = 'b')
#axs[jobs].scatter([ii['events_ALICE'] for ii in dict_bound['1_generation']],[ii['events_LHCb'] for ii in dict_bound['1_generation']], color = 'g')
# %%

ALICE = [ii['events_ALICE'] for ii in dict_bound['3_generation']]
idx_ALICE = np.where( ALICE== 1737.0*np.ones(len(ALICE)))
LHCb = [ii['events_LHCb'] for ii in dict_bound['3_generation']]
idx_LHCb = np.where( LHCb== 1735.0*np.ones(len(LHCb)))


print(dict_bound['3_generation'][idx_ALICE[0][np.argmax([LHCb[int(ii)] for ii in idx_ALICE[0]])]])
print(dict_bound['3_generation'][idx_LHCb[0][np.argmax([ALICE[int(ii)] for ii in idx_LHCb[0]])]])

print(dict_bound['3_generation'][idx_ALICE[0][np.argmax([LHCb[int(ii)] for ii in idx_ALICE[0]])]]['slots_to_shift'])
print(dict_bound['3_generation'][idx_LHCb[0][np.argmax([ALICE[int(ii)] for ii in idx_LHCb[0]])]]['slots_to_shift'])

# %%
